package com.codewolf.commonserver;

import com.codewolf.commonserver.model.entity.SysUser;
import com.codewolf.commonserver.service.SysMenuService;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class CommonServerApplicationTests {

    @Autowired
    private  SysMenuService menuService;

    @Test
    void contextLoads() {
        this.getListRoutes();
    }

    void getListRoutes() {
        SysUser user1 = new SysUser().setId(1);
        SysUser user2 = new SysUser().setId(2);
        ArrayList<SysUser> sysUsers = new ArrayList<>();
        sysUsers.add(user1);
        sysUsers.add(user2);
        List<SysUser> collect = sysUsers.stream().map(u -> u.setId(3)).collect(Collectors.toList());
        System.out.println(collect);

    }
}
