package com.codewolf.commonserver.core.security.model;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.codewolf.commonserver.model.bo.UserAuthInfoBo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @ClassName SysUserDetails
 * @Description 用户信息实体类
 * @Author code wolf
 * @Date 2023/12/31 12:20
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class SysUserDetails implements UserDetails {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long userId;
    private String username;
    private String password;
    private Boolean enabled;
    private Long deptId;
    private Integer dataScope;
    private Collection<SimpleGrantedAuthority> authorities;
    private Set<String> perms;
    private Set<String> roles;

    public SysUserDetails(UserAuthInfoBo userAuthInfo) {
        this.userId = userAuthInfo.getUserId();
        this.username = userAuthInfo.getUsername();
        this.password = userAuthInfo.getPassword();
        this.enabled = userAuthInfo.getEnabled();
        this.perms = userAuthInfo.getPerms();
        this.roles = userAuthInfo.getRoles();
        Set<String> roles = userAuthInfo.getRoles();
        if (CollectionUtil.isNotEmpty(roles)) {
            this.authorities = roles.stream()
                    .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                    .collect(Collectors.toSet());
        } else {
            this.authorities = Collections.emptySet();
        }
    }

    @Override
    public Collection<SimpleGrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
