package com.codewolf.commonserver.core.security.service;

import com.codewolf.commonserver.core.security.model.SysUserDetails;
import com.codewolf.commonserver.model.bo.UserAuthInfoBo;
import com.codewolf.commonserver.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @ClassName SysUserDetailService
 * @Description 获取用户信息
 * @Author code wolf
 * @Date 2023/12/31 12:18
 * @Version 1.0
 */
@Service
@RequiredArgsConstructor
public class SysUserDetailsService implements UserDetailsService {

    private final SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAuthInfoBo userAuthInfo = sysUserService.getUserAuthInfo(username);
        if(userAuthInfo == null) {
            throw new UsernameNotFoundException(username);
        }
        return new SysUserDetails(userAuthInfo);
    }
}

