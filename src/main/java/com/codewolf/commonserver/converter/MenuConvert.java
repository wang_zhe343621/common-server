package com.codewolf.commonserver.converter;

import com.codewolf.commonserver.model.entity.SysMenu;
import com.codewolf.commonserver.model.vo.MenuFormVo;
import com.codewolf.commonserver.model.vo.MenuSelectionTreeNodeVo;
import com.codewolf.commonserver.model.vo.MenuTreeNodeVo;
import org.mapstruct.Mapper;

/**
 * @ClassName MenuConvert
 * @Description 菜单实体转换器
 * @Author code wolf
 * @Date 2024/1/24 22:29
 * @Version 1.0
 */
@Mapper(componentModel = "spring")
public interface MenuConvert {
    MenuTreeNodeVo entityToMenuTreeNodeVo(SysMenu menu);

    MenuSelectionTreeNodeVo entityToMenuSelectionTreeNodeVo(SysMenu menu);

    MenuFormVo entityToMenuFormVo(SysMenu menu);

    SysMenu menuFormVoToEntity(MenuFormVo menuFormVo);
}
