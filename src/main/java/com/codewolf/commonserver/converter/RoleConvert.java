package com.codewolf.commonserver.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.codewolf.commonserver.model.entity.SysRole;
import com.codewolf.commonserver.model.param.RoleInsertVo;
import com.codewolf.commonserver.model.param.RoleUpdateVo;
import com.codewolf.commonserver.model.vo.RoleFormVo;
import com.codewolf.commonserver.model.vo.RolePageVo;
import org.mapstruct.Mapper;

/**
 * @ClassName RoleConvert
 * @Description 角色对象转换器
 * @Author code wolf
 * @Date 2024/1/15 12:41
 * @Version 1.0
 */
@Mapper(componentModel = "spring")
public interface RoleConvert {
    Page<RolePageVo> entityToPage(Page<SysRole> page);

    RoleFormVo entityToRoleFormVo(SysRole role);

    SysRole roleUpdateVoToEntity(RoleUpdateVo roleUpdateVo);

    SysRole roleInsertVoToEntity(RoleInsertVo roleInsertVo);
}
