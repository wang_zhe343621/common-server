package com.codewolf.commonserver.converter;

import com.codewolf.commonserver.model.entity.SysDept;
import com.codewolf.commonserver.model.vo.DeptFormVo;
import com.codewolf.commonserver.model.vo.DeptNodeVo;
import com.codewolf.commonserver.model.vo.DeptTreeOptionVo;
import org.mapstruct.Mapper;

/**
 * @ClassName DeptConvert
 * @Description 部门实体转换
 * @Author code wolf
 * @Date 2024/1/22 21:34
 * @Version 1.0
 */
@Mapper(componentModel = "spring")
public interface DeptConvert {
    DeptNodeVo entityToDeptNodeVo(SysDept dept);

    SysDept DeptFormVoToEntity(DeptFormVo deptFormVo);

    DeptFormVo entityToDeptFormVo(SysDept dept);

    DeptTreeOptionVo entityToDeptTreeOptionVo(SysDept dept);
}
