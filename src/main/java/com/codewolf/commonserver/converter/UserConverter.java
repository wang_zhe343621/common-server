package com.codewolf.commonserver.converter;


import com.codewolf.commonserver.model.entity.SysUser;
import com.codewolf.commonserver.model.vo.UserInfoVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @ClassName UserConvert
 * @Description 用户对象转换器
 * @Author code wolf
 * @Date 2024/1/4 15:46
 * @Version 1.0
 */
@Mapper(componentModel = "spring")
public interface UserConverter {

    @Mappings({
            @Mapping(target = "userId", source = "id")
    })
    UserInfoVo toUserInfoVo(SysUser user);
}
