package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.dto.LoginResult;

/**
 * @ClassName AuthService
 * @Description 认证服务接口
 * @Author code wolf
 * @Date 2023/12/31 14:33
 * @Version 1.0
 */
public interface AuthService {

    LoginResult login(String username, String password);
}
