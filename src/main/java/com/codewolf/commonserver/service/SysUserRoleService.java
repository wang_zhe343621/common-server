package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
