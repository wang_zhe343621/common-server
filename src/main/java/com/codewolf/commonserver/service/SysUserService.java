package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.bo.UserAuthInfoBo;
import com.codewolf.commonserver.model.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.codewolf.commonserver.model.vo.UserInfoVo;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysUserService extends IService<SysUser> {
    UserAuthInfoBo getUserAuthInfo(String username);

    /**
     * 获取登录用户信息
     */
    UserInfoVo getCurrentUserInfo();
}
