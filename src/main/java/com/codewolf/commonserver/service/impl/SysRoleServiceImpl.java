package com.codewolf.commonserver.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.codewolf.commonserver.common.constant.SystemConstant;
import com.codewolf.commonserver.common.util.SecurityUtil;
import com.codewolf.commonserver.converter.RoleConvert;
import com.codewolf.commonserver.mapper.SysRoleMenuMapper;
import com.codewolf.commonserver.mapper.SysUserRoleMapper;
import com.codewolf.commonserver.model.entity.SysRole;
import com.codewolf.commonserver.mapper.SysRoleMapper;
import com.codewolf.commonserver.model.entity.SysRoleMenu;
import com.codewolf.commonserver.model.param.RoleInsertVo;
import com.codewolf.commonserver.model.param.RoleUpdateVo;
import com.codewolf.commonserver.model.vo.RoleFormVo;
import com.codewolf.commonserver.model.vo.RolePageVo;
import com.codewolf.commonserver.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    private final RoleConvert roleConvert;
    private final SysUserRoleMapper userRoleMapper;
    private final SysRoleMapper roleMapper;
    private final SysRoleMenuMapper roleMenuMapper;

    @Override
    public Page<RolePageVo> getRolesPage(String keyword, Integer pageSize, Integer pageNum) {
        Page<SysRole> page = this.page(new Page<>(pageNum, pageSize),
                new LambdaQueryWrapper<>(SysRole.class)
                        .and(StrUtil.isNotBlank(keyword),
                                wrapper ->
                                        wrapper.like(SysRole::getName, keyword)
                                                .or()
                                                .like(SysRole::getCode, keyword)
                        )
                        .ne(!SecurityUtil.isRoot(), SysRole::getCode, SystemConstant.ROOT_ROLE_CODE)
                        .orderByAsc(SysRole::getSort)

        );
        return roleConvert.entityToPage(page);

    }

    /**
     * 删除角色
     *
     * @param ids 角色id 字符串
     * @return
     */
    @Override
    public void deleteRoles(String ids) {
        List<Long> roleIds = Arrays.stream(ids.split(",")).map(Long::parseLong).toList();
        roleIds.forEach(id -> {
            SysRole role = this.getById(id);
            Assert.notNull(role, "角色不存在");
            boolean roleAssignedToUser = this.isRoleAssignedToUser(role);
            Assert.isFalse(roleAssignedToUser, "角色【{}】已分配用户，请先解除关联后删除", role.getName());
            boolean deleteResult = this.removeById(role.getId());
            if (deleteResult) {
                // 删除成功，刷新权限缓存
                System.out.println("删除成功，刷新权限缓存");
            }
        });

    }

    /**
     * 角色是否被分配给用户
     *
     * @param role
     * @return
     */
    private boolean isRoleAssignedToUser(SysRole role) {
        int count = userRoleMapper.countUsersForRole(role.getId());
        return count > 0;
    }

    /**
     * 角色表单数据
     *
     * @param roleId 角色id
     */
    @Override
    public RoleFormVo getRoleForm(Long roleId) {
        SysRole role = this.getById(roleId);
        Assert.notNull(role, "系统错误");
        return roleConvert.entityToRoleFormVo(role);
    }

    /**
     * 更新角色
     *
     * @param roleUpdateVo 更新角色实体
     */
    @Override
    public void updateRole(RoleUpdateVo roleUpdateVo) {
        SysRole sysRole = roleConvert.roleUpdateVoToEntity(roleUpdateVo);
        boolean isRoleRepeat = this.isRoleRepeat(sysRole.getId(), sysRole.getName(), sysRole.getCode());
        Assert.isFalse(isRoleRepeat, "角色名称或者角色编码重复，请检查！");
        this.updateById(sysRole);

    }

    /**
     * 新增角色
     *
     * @param roleInsertVo 新增角色实体
     */
    @Override
    public void insertRole(RoleInsertVo roleInsertVo) {
        SysRole sysRole = roleConvert.roleInsertVoToEntity(roleInsertVo);
        boolean isRoleRepeat = this.isRoleRepeat(sysRole.getId(), sysRole.getName(), sysRole.getCode());
        Assert.isFalse(isRoleRepeat, "角色名称或者角色编码重复，请检查！");
        this.save(sysRole);
    }

    /**
     * 校验角色是否重复
     *
     * @param roleId   更新角色id
     * @param roleName 角色名称
     * @param roleCode 角色编码
     * @return
     */
    private boolean isRoleRepeat(Long roleId, String roleName, String roleCode) {
        long count = this.count(new LambdaQueryWrapper<SysRole>()
                .ne(roleId != null, SysRole::getId, roleId)
                .and(wrapper -> wrapper.eq(SysRole::getCode, roleCode).or().eq(SysRole::getName, roleName))
        );

        return count > 0;
    }

    /**
     * 获取角色菜单id集合
     * @param roleId 角色id
     * @return
     */
    @Override
    public List<Long> getRoleMenuIds(Long roleId) {
        return roleMapper.getRoleMenuIds(roleId);
    }


}
