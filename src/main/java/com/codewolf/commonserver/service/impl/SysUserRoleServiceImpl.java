package com.codewolf.commonserver.service.impl;

import com.codewolf.commonserver.model.entity.SysUserRole;
import com.codewolf.commonserver.mapper.SysUserRoleMapper;
import com.codewolf.commonserver.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
