package com.codewolf.commonserver.service.impl;

import com.codewolf.commonserver.model.entity.SysDictType;
import com.codewolf.commonserver.mapper.SysDictTypeMapper;
import com.codewolf.commonserver.service.SysDictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

}
