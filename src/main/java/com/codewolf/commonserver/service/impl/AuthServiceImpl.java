package com.codewolf.commonserver.service.impl;

import com.codewolf.commonserver.core.security.jwt.JwtTokenProvider;
import com.codewolf.commonserver.model.dto.LoginResult;
import com.codewolf.commonserver.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * @ClassName AuthServiceImpl
 * @Description 认证服务 实现类
 * @Author code wolf
 * @Date 2023/12/31 14:34
 * @Version 1.0
 */

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public LoginResult login(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username.toLowerCase(Locale.ROOT).trim(), password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        String token = jwtTokenProvider.createToken(authentication);
        return LoginResult.builder().tokenType("Bearer").accessToken(token).build();
    }
}
