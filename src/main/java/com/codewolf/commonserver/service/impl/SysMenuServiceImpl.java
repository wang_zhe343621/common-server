package com.codewolf.commonserver.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.codewolf.commonserver.common.constant.SystemConstant;
import com.codewolf.commonserver.common.util.CommonUtil;
import com.codewolf.commonserver.common.util.SecurityUtil;
import com.codewolf.commonserver.converter.MenuConvert;
import com.codewolf.commonserver.model.bo.RouteBo;
import com.codewolf.commonserver.model.common.MenuBase;
import com.codewolf.commonserver.model.common.TreeNodeBase;
import com.codewolf.commonserver.model.entity.SysMenu;
import com.codewolf.commonserver.mapper.SysMenuMapper;
import com.codewolf.commonserver.model.vo.*;
import com.codewolf.commonserver.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 菜单管理 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    private final SysMenuMapper menuMapper;
    private final MenuConvert menuConvert;

    @Override
    public List<RouteVo> listRoutes() {
        Long userId = SecurityUtil.getUserId();
        List<RouteBo> listRoutes = menuMapper.getListRoutes(userId);
        return buildMenuTree(SystemConstant.ROOT_ID, listRoutes, (this::toRouteVo), Comparator.comparingInt(RouteVo::getSort));
//        return buildRoute(SystemConstant.ROOT_ID, listRoutes);
    }

    /**
     * 生成vue路由树
     */
    private List<RouteVo> buildRoute(Long RouteId, List<RouteBo> menuList) {
        List<RouteVo> routeVoList = new ArrayList<>();
        menuList.forEach(routeBo -> {
            if (Objects.equals(routeBo.getParentId(), RouteId)) {
                RouteVo routeVo = toRouteVo(routeBo);
                List<RouteVo> children = buildRoute(routeBo.getId(), menuList);
                routeVo.setChildren(children);
                routeVoList.add(routeVo);
            }
        });
        routeVoList.sort((a, b) -> {
            return a.getSort() - b.getSort();
        });
        return routeVoList;

    }

    /**
     * RouteBo to RouteVo
     */
    private RouteVo toRouteVo(RouteBo routeBo) {
        RouteVo routeVo = new RouteVo();
        routeVo.setPath(routeBo.getPath());
        routeVo.setName(routeBo.getName());
        routeVo.setRedirect(routeBo.getRedirect());
        routeVo.setComponent(routeBo.getComponent());
        routeVo.setSort(routeBo.getSort());
        RouteVo.Meta meta = new RouteVo.Meta();
        meta.setTitle(routeBo.getName());
        meta.setIcon(routeBo.getIcon());
        meta.setAlwaysShow(routeBo.getAlwaysShow());
        meta.setKeepAlive(routeBo.getKeepAlive());
        meta.setVisible(routeBo.getVisible());
        routeVo.setMeta(meta);
        return routeVo;
    }

    /**
     * 获取菜单配置项
     */
    @Override
    public List<MenuOptionVo> getMenuOption() {
        List<SysMenu> menuList = this.list();
        return buildMenuTree(SystemConstant.ROOT_ID, menuList, (source) -> {
            MenuOptionVo menuOptionVo = new MenuOptionVo();
            menuOptionVo.setId((source.getId()));
            menuOptionVo.setName(source.getName());
            menuOptionVo.setSort(source.getSort());
            return menuOptionVo;
        }, Comparator.comparingInt(MenuOptionVo::getSort));

    }

    /**
     * 生成菜单树
     */
    private <T extends TreeNodeBase, K extends MenuBase> List<T> buildMenuTree(Long rootId, List<K> menuList, MenuTreeItemConvert<T, K> menuConvert, Comparator<T> comparator) {
        List<T> resultList = new ArrayList<>();
        menuList.forEach(item -> {
            if (Objects.equals(item.getMenuParentId(), rootId)) {
                List<T> children = buildMenuTree(item.getMenuId(), menuList, menuConvert, comparator);
                T result = menuConvert.convert(item);
                result.setChildren(children);
                resultList.add(result);
            }
        });
        if (comparator != null) {
            resultList.sort(comparator);
        }
        return resultList;
    }

    /**
     * 树形菜单项转换
     *
     * @param <T>
     * @param <K>
     */
    private interface MenuTreeItemConvert<T, K> {
        T convert(K source);
    }

    /**
     * 获取菜单树形结构数据
     *
     * @param keyword 查询条件：菜单名称
     * @return 菜单树形节点视图实体列表
     */
    @Override
    public List<MenuTreeNodeVo> getTreeList(String keyword) {
        List<SysMenu> menuList = this.list(new LambdaQueryWrapper<SysMenu>()
                .like(ObjUtil.isNotEmpty(keyword), SysMenu::getName, keyword));
        return CommonUtil.buildTree(
                SystemConstant.MENU_ROOT_ID,
                menuList,
                menuConvert::entityToMenuTreeNodeVo,
                Comparator.comparingInt(MenuTreeNodeVo::getSort));
    }

    /**
     * 获取菜单树形结构数据（带有顶级菜单）
     *
     * @return 菜单下拉选择树形节点视图实体列表
     */
    @Override
    public List<MenuSelectionTreeNodeVo> getSelectionTreeList() {
        List<SysMenu> menuList = this.list();
        List<MenuSelectionTreeNodeVo> rootNodeChildren = CommonUtil.buildTree(
                SystemConstant.MENU_ROOT_ID,
                menuList,
                menuConvert::entityToMenuSelectionTreeNodeVo,
                Comparator.comparingInt(MenuSelectionTreeNodeVo::getSort));
        MenuSelectionTreeNodeVo rootNode = new MenuSelectionTreeNodeVo();
        rootNode.setId(SystemConstant.MENU_ROOT_ID);
        rootNode.setSort(0);
        rootNode.setName("顶级菜单");
        rootNode.setChildren(rootNodeChildren);
        return List.of(rootNode);
    }

    /**
     * 新增菜单
     *
     * @param menuFormVo 菜单表单视图
     */
    @Override
    public void addMenu(MenuFormVo menuFormVo) {
        SysMenu sysMenu = menuConvert.menuFormVoToEntity(menuFormVo);
        long replaceNameCount = this.count(new LambdaQueryWrapper<>(SysMenu.class).eq(SysMenu::getName, sysMenu.getName()));
        Assert.isTrue(replaceNameCount == 0, "菜单名称重复");
        sysMenu.setTreePath(generateTreePath(sysMenu));
        this.save(sysMenu);
    }

    /**
     * 更新菜单
     *
     * @param menuFormVo 菜单表单视图实体
     */
    @Override
    public void updateMenu(MenuFormVo menuFormVo) {
        //判断菜单名称重复
        SysMenu sysMenu = menuConvert.menuFormVoToEntity(menuFormVo);
        long replaceNameCount = this.count(new LambdaQueryWrapper<>(SysMenu.class).ne(SysMenu::getId, sysMenu.getId()).eq(SysMenu::getName, sysMenu.getName()));
        Assert.isTrue(replaceNameCount == 0, "菜单名称重复");

        sysMenu.setTreePath(generateTreePath(sysMenu));
        this.updateById(sysMenu);
    }

    /**
     * 生成treePath
     *
     * @param sysMenu 菜单实体
     * @return 菜单树路径
     */
    private String generateTreePath(SysMenu sysMenu) {
        //生成treePath
        SysMenu sysMenuParent = this.getById(sysMenu.getParentId());
        String treePath = null;
        if (ObjUtil.isNotNull(sysMenuParent)) {
            treePath = sysMenuParent.getTreePath() + "," + sysMenuParent.getId();
        } else {
            treePath = String.valueOf(SystemConstant.MENU_ROOT_ID);
        }
        return treePath;
    }

    /**
     * 删除菜单（如果是目录，会删除目录下的所有菜单）
     *
     * @param menuId 菜单id
     */
    @Override
    public void deleteMenu(Long menuId) {
        this.remove(new LambdaQueryWrapper<>(SysMenu.class)
                .eq(SysMenu::getId, menuId)
                .or()
                .apply("CONCAT(',',parent_id,',') LIKE CONCAT('%,',{0},',%')", menuId));
    }

    /**
     * 获取菜单表单数据
     *
     * @param menuId 菜单id
     * @return 菜单表单实体
     */
    @Override
    public MenuFormVo getMenuForm(Long menuId) {
        SysMenu sysMenu = this.getById(menuId);
        Assert.notNull(sysMenu, "菜单不存在");
        return menuConvert.entityToMenuFormVo(sysMenu);
    }
}