package com.codewolf.commonserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.codewolf.commonserver.common.util.SecurityUtil;
import com.codewolf.commonserver.converter.UserConverter;
import com.codewolf.commonserver.mapper.SysMenuMapper;
import com.codewolf.commonserver.model.bo.UserAuthInfoBo;
import com.codewolf.commonserver.model.entity.SysUser;
import com.codewolf.commonserver.mapper.SysUserMapper;
import com.codewolf.commonserver.model.vo.UserInfoVo;
import com.codewolf.commonserver.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    private final UserConverter userConverter;
    private final SysMenuMapper menuMapper;

    @Override
    public UserAuthInfoBo getUserAuthInfo(String username) {
        UserAuthInfoBo userAuthInfo = this.baseMapper.getUserAuthInfo(username);
        if (userAuthInfo != null) {
            //获取用户权限

        }
        return userAuthInfo;
    }

    /**
     * 获取登录用户信息
     *
     * @return {@link UserInfoVo} 用户信息
     */
    @Override
    public UserInfoVo getCurrentUserInfo() {
        Long userId = SecurityUtil.getUserId();

        SysUser user = this.getOne(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getId, userId)
                .select(
                        SysUser::getId,
                        SysUser::getUsername,
                        SysUser::getNickname,
                        SysUser::getAvatar
                )
        );
        UserInfoVo userInfoVo = userConverter.toUserInfoVo(user);
        //用户角色集合
        Set<String> roles = SecurityUtil.getRoles();
        userInfoVo.setRoles(roles);

        //用户权限集合
        Set<String> perms = menuMapper.getPermsListByRoles(roles);
        userInfoVo.setPerms(perms);
        return userInfoVo;
    }
}
