package com.codewolf.commonserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.codewolf.commonserver.model.entity.SysRoleMenu;
import com.codewolf.commonserver.mapper.SysRoleMenuMapper;
import com.codewolf.commonserver.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    /**
     * 分配菜单给角色
     * @param roleId 角色id
     * @param menuIds 菜单id集合
     */
    @Override
    public void updateRoleMenus(Long roleId, List<Long> menuIds) {
        this.remove(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId,roleId));
        List<SysRoleMenu> sysRoleMenus = menuIds.stream().map(menuId -> SysRoleMenu.builder().roleId(roleId).menuId(menuId).build()).toList();
        this.saveBatch(sysRoleMenus);
    }
}
