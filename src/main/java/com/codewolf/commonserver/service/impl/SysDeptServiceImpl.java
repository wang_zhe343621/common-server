package com.codewolf.commonserver.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.codewolf.commonserver.common.constant.SystemConstant;
import com.codewolf.commonserver.common.util.CommonUtil;
import com.codewolf.commonserver.common.util.SecurityUtil;
import com.codewolf.commonserver.converter.DeptConvert;
import com.codewolf.commonserver.model.entity.SysDept;
import com.codewolf.commonserver.mapper.SysDeptMapper;
import com.codewolf.commonserver.model.vo.DeptFormVo;
import com.codewolf.commonserver.model.vo.DeptNodeVo;
import com.codewolf.commonserver.model.vo.DeptTreeOptionVo;
import com.codewolf.commonserver.service.SysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
@RequiredArgsConstructor
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    private final DeptConvert deptConvert;

    /**
     * 查询树形部门列表
     *
     * @param keyword    部门名称
     * @param deptStatus 部门状态
     * @return 树形部门列表
     */
    @Override
    public List<DeptNodeVo> getDeptList(String keyword, Byte deptStatus) {
        List<SysDept> sysDeptList = this.list(
                new LambdaQueryWrapper<SysDept>()
                        .like(ObjUtil.isNotEmpty(keyword), SysDept::getName, keyword)
                        .and(ObjUtil.isNotNull(deptStatus), wrapper ->
                                wrapper.eq(SysDept::getStatus, deptStatus)
                        ));
        Set<Long> idSet = sysDeptList.stream().map(SysDept::getId).collect(Collectors.toSet());
        Set<Long> parentIdSet = sysDeptList.stream().map(SysDept::getParentId).collect(Collectors.toSet());
        List<Long> rootIdList = CollUtil.subtractToList(parentIdSet, idSet);
        return rootIdList.stream()
                .flatMap(rootId -> CommonUtil.buildTree(rootId
                        , sysDeptList
                        , deptConvert::entityToDeptNodeVo
                        , Comparator.comparingInt(DeptNodeVo::getSort)).stream())
                .sorted(Comparator.comparingInt(DeptNodeVo::getSort))
                .toList();
    }

    /**
     * 新增部门
     *
     * @param deptFormVo 部门表单视图对象
     */
    @Override
    public void insertDept(DeptFormVo deptFormVo) {
        SysDept sysDept = deptConvert.DeptFormVoToEntity(deptFormVo);
        long replaceNameCount = this.count(new LambdaQueryWrapper<SysDept>()
                .eq(SysDept::getName, sysDept.getName())
                .and(ObjUtil.isNotNull(sysDept.getId()), wrapper ->
                        wrapper.ne(SysDept::getId, sysDept.getId())
                ));
        Assert.isTrue(replaceNameCount == 0, "部门名称重复");
        sysDept.setCreateBy(SecurityUtil.getUserId());
        //生成tree_path
        setTreePath(sysDept);
        this.save(sysDept);
    }

    /**
     * 更新部门
     *
     * @param deptFormVo 部门表单视图对象
     */
    @Override
    public void updateDept(DeptFormVo deptFormVo) {
        SysDept sysDept = deptConvert.DeptFormVoToEntity(deptFormVo);
        long replaceNameCount = this.count(new LambdaQueryWrapper<SysDept>()
                .eq(SysDept::getName, sysDept.getName())
                .and(ObjUtil.isNotNull(sysDept.getId()), wrapper ->
                        wrapper.ne(SysDept::getId, sysDept.getId())
                ));
        Assert.isTrue(replaceNameCount == 0, "部门名称重复");
        sysDept.setUpdateBy(SecurityUtil.getUserId());
        //生成tree_path
        setTreePath(sysDept);
        this.saveOrUpdate(sysDept);
    }

    /**
     * 设置部门的tree_path
     *
     * @param sysDept 部门实体
     */
    private void setTreePath(SysDept sysDept) {
        SysDept deptParent = this.getOne(new LambdaQueryWrapper<SysDept>().eq(SysDept::getId, sysDept.getParentId()));
        String treePath = "";
        if (ObjUtil.isNotNull(deptParent)) {
            treePath = deptParent.getTreePath() + "," + deptParent.getId();
        } else {
            treePath = String.valueOf(sysDept.getParentId());
        }
        sysDept.setTreePath(treePath);
    }

    /**
     * 删除部门
     *
     * @param ids 部门id合集字符串(以英文逗号分隔)
     */
    @Override
    public void deleteDepts(String ids) {
        String[] idArray = ids.split(",");
        for (String id : idArray) {
            this.remove(new LambdaQueryWrapper<SysDept>()
                    .eq(SysDept::getId, id)
                    .or()
                    .apply("CONCAT(',',tree_path,',') like CONCAT('%,',{0},',%')", id));
        }
    }

    /**
     * 获取部门表单数据
     * @param deptId 部门id
     * @return 部门表单实体
     */
    @Override
    public DeptFormVo getDeptForm(Long deptId) {
        SysDept byId = this.getById(deptId);
        return deptConvert.entityToDeptFormVo(byId);
    }

    /**
     * 获取部门树形结构数据
     */
    @Override
    public List<DeptTreeOptionVo> getDeptTreeSelectOption() {
        List<SysDept> deptList = this.list();
        List<DeptTreeOptionVo> topNodeChildren = CommonUtil.buildTree(SystemConstant.DEPT_ROOT_ID, deptList, deptConvert::entityToDeptTreeOptionVo, Comparator.comparingInt(DeptTreeOptionVo::getSort));
        DeptTreeOptionVo topNode = new DeptTreeOptionVo();
        topNode.setId(SystemConstant.DEPT_ROOT_ID);
        topNode.setName("顶级部门");
        topNode.setSort(0);
        topNode.setChildren(topNodeChildren);
        return List.of(topNode);
    }
}
