package com.codewolf.commonserver.service.impl;

import com.codewolf.commonserver.model.entity.SysDict;
import com.codewolf.commonserver.mapper.SysDictMapper;
import com.codewolf.commonserver.service.SysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

}
