package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {
    /**
     * 分配菜单给角色
     * @param roleId 角色id
     * @param menuIds 菜单id集合
     */
    void updateRoleMenus(Long roleId, List<Long> menuIds);
}
