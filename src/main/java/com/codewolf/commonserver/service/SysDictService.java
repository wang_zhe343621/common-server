package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysDictService extends IService<SysDict> {

}
