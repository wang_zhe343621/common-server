package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysDictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysDictTypeService extends IService<SysDictType> {

}
