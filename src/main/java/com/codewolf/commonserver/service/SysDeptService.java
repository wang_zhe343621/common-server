package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.codewolf.commonserver.model.vo.DeptFormVo;
import com.codewolf.commonserver.model.vo.DeptNodeVo;
import com.codewolf.commonserver.model.vo.DeptTreeOptionVo;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 查询部门列表
     * @param keyword 关键字
     * @param deptStatus 部门状态
     * @return 部门列表树结构
     */
    List<DeptNodeVo> getDeptList(String keyword, Byte deptStatus);

    /**
     * 新增部门
     * @param deptFormVo 部门表单视图对象
     */
    void insertDept(DeptFormVo deptFormVo);

    /**
     * 更新部门
     * @param deptFormVo 部门表单视图对象
     */
    void updateDept(DeptFormVo deptFormVo);

    /**
     * 删除部门
     * @param ids 部门id合集字符串(以英文逗号分隔)
     */
    void deleteDepts(String ids);

    /**
     * 获取部门表单数据
     * @param deptId 部门id
     * @return 部门表单数据
     */
    DeptFormVo getDeptForm(Long deptId);

    /**
     * 获取部门树形下拉选项
     * @return 部门树形结构数据
     */
    List<DeptTreeOptionVo> getDeptTreeSelectOption();
}
