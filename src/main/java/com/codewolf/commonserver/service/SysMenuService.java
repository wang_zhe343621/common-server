package com.codewolf.commonserver.service;

import com.codewolf.commonserver.model.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.codewolf.commonserver.model.vo.*;

import java.util.List;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * vue 路由列表
     */
    List<RouteVo> listRoutes();

    /**
     * 菜单配置项
     */
    List<MenuOptionVo> getMenuOption();

    /**
     * 获取菜单树形数据
     */
    List<MenuTreeNodeVo> getTreeList(String keyword);

    /**
     * 获取菜单下拉树形结构数据（带有顶级菜单）
     */
    List<MenuSelectionTreeNodeVo> getSelectionTreeList();

    /**
     * 新增菜单
     * @param menuFormVo 菜单表单视图实体
     */
    void addMenu(MenuFormVo menuFormVo);

    /**
     * 更新菜单
     * @param menuFormVo 菜单表单视图实体
     */
    void updateMenu(MenuFormVo menuFormVo);

    /**
     * 删除菜单
     * @param menuId 菜单id
     */
    void deleteMenu(Long menuId);

    /**
     * 获取菜单表单数据
     * @param menuId 菜单id
     * @return 菜单表单实体
     */
    MenuFormVo getMenuForm(Long menuId);
}
