package com.codewolf.commonserver.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.codewolf.commonserver.model.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.codewolf.commonserver.model.param.RoleInsertVo;
import com.codewolf.commonserver.model.param.RoleUpdateVo;
import com.codewolf.commonserver.model.vo.RoleFormVo;
import com.codewolf.commonserver.model.vo.RolePageVo;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
public interface SysRoleService extends IService<SysRole> {

    Page<RolePageVo> getRolesPage(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 删除角色
     */
    void deleteRoles(String ids);

    /**
     * 角色表单数据
     * @param roleId 角色id
     */
    RoleFormVo getRoleForm(Long roleId);

    /**
     * 更新角色
     * @param roleUpdateVo 更新角色实体
     */
    void updateRole(RoleUpdateVo roleUpdateVo);

    /**
     * 新增角色
     * @param roleInsertVo 新增角色实体
     */
    void insertRole(RoleInsertVo roleInsertVo);

    /**
     * 获取角色所分配的菜单id
     * @param roleId 角色id
     */
    List<Long> getRoleMenuIds(Long roleId);


}
