package com.codewolf.commonserver.model.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @ClassName RoleInsertVo
 * @Description 角色新增实体
 * @Author code wolf
 * @Date 2024/1/17 16:08
 * @Version 1.0
 */
@Schema(name="角色新增实体")
@Data
public class RoleInsertVo {

    @Schema(description = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String name;

    @Schema(description = "角色编码")
    @NotBlank(message = "角色编码不能为空")
    private String code;

    @Schema(description = "显示顺序")
    @NotNull(message = "显示顺序不能为空")
    private Integer sort;

    @Schema(description = "角色状态(1-正常；0-停用)")
    @NotNull(message = "角色状态不能为空")
    private Boolean status;

    @Schema(description = "数据权限(0-所有数据；1-部门及子部门数据；2-本部门数据；3-本人数据)")
    @NotNull(message = "数据权限不能为空")
    private Byte dataScope;
}
