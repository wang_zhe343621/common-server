package com.codewolf.commonserver.model.param;

import com.codewolf.commonserver.common.base.BasePageQuery;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName RoleQueryPageVo
 * @Description 角色分页查询入参
 * @Author code wolf
 * @Date 2024/1/14 21:26
 * @Version 1.0
 */
@Schema(name = "角色分页查询入参实体")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleQueryPageVo extends BasePageQuery {
    @Schema(description = "关键词")
    private String keyword;
}
