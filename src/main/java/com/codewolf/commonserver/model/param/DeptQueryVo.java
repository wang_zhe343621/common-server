package com.codewolf.commonserver.model.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName DeptQueryVo
 * @Description 部门列表查询条件实体
 * @Author code wolf
 * @Date 2024/1/22 16:37
 * @Version 1.0
 */
@Schema(name = "部门列表查询条件")
@Data
public class DeptQueryVo {

    @Schema(description = "关键字")
    private String keyword;
    @Schema(description = "部门状态")
    //todo 编写校验 部门状态 为 整数，可以为空，只能是1或者2;抽取出来
    private Byte deptStatus;
}
