package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.codewolf.commonserver.common.validator.groups.Save;
import com.codewolf.commonserver.common.validator.groups.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @ClassName DeptFormVO
 * @Description 部门表单实体
 * @Author code wolf
 * @Date 2024/1/23 13:27
 * @Version 1.0
 */
@Schema(name="部门表单实体")
@Data
public class DeptFormVo {
    @Schema(description = "主键")
    @NotNull(message = "主键不能为空",groups = {Update.class})
    private Long id;

    @Schema(description = "父节点id")
    @NotNull(message = "父节点id不能为空",groups = {Save.class,Update.class})
    private Long parentId;

    @Schema(description = "部门名称")
    @NotBlank(message = "部门名称不能为空",groups = {Save.class,Update.class})
    private String name;

    @Schema(description = "显示顺序")
    @NotNull(message = "显示顺序不能为空",groups = {Save.class,Update.class})
    private Integer sort;

    @Schema(description = "状态(1:正常;0:禁用)")
    @NotNull(message = "状态不能为空",groups = {Save.class,Update.class})
    private Byte status;


}
