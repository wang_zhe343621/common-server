package com.codewolf.commonserver.model.vo;

import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName MenuOptionVo
 * @Description 菜单配置项实体
 * @Author code wolf
 * @Date 2024/1/20 18:20
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Schema(name="菜单配置项实体")
@Data
public class MenuOptionVo extends TreeNodeBase {

    private Long id;
    private String name;
    private Integer sort;

}
