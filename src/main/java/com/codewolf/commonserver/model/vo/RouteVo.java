package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @ClassName RouteVo
 * @Description 路由视图
 * @Author code wolf
 * @Date 2024/1/8 0:34
 * @Version 1.0
 */
@Schema(name = "路由视图")
@Data
public class RouteVo extends TreeNodeBase {
    @Schema(name="路径")
    private String path;
    private String name;
    private String redirect;
    private String component;
    private Integer sort;
//    private List<RouteVo> children;
    private Meta meta;
    @Schema(name = "路由元信息")
    @Data
    public static class Meta {
        @Schema(description = "菜单标题")
        private String title;
        @Schema(description = "图标class")
        private String icon;

        @Schema(description = "【目录】只有一个子路由是否始终显示(1:是 0:否)")
        private Byte alwaysShow;

        @Schema(description = "【菜单】是否开启页面缓存(1:是 0:否)")
        private Byte keepAlive;

        @Schema(description = "显示状态(1-显示;0-隐藏)")
        private Boolean visible;
    }

}

