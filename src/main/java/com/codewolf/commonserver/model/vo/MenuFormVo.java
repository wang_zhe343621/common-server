package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.codewolf.commonserver.common.validator.groups.MenuFormVoGroupSequenceProvider;
import com.codewolf.commonserver.common.validator.groups.Save;
import com.codewolf.commonserver.common.validator.groups.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.Data;
import org.hibernate.validator.group.GroupSequenceProvider;

/**
 * @ClassName MenuFormVo
 * @Description 菜单表单视图实体
 * @Author code wolf
 * @Date 2024/1/24 19:53
 * @Version 1.0
 */
@Schema(name="菜单表单视图实体")
@Data
@GroupSequenceProvider(MenuFormVoGroupSequenceProvider.class)
public class MenuFormVo {

    @Schema(description = "菜单ID")
    @NotNull(message = "菜单id不能为空",groups = {Update.class})
    @Null(message = "菜单id为空",groups = {Save.class})
    private Long id;

    @Schema(description = "父菜单ID")
    @NotNull(message = "父菜单ID不能为空")
    private Long parentId;


    @Schema(description = "菜单名称")
    @NotNull(message = "菜单名称不能为空")
    private String name;

    @Schema(description = "菜单类型(1:菜单 2:目录 3:外链 4:按钮)")
    @NotNull(message = "菜单类型不能为空")
    private Byte type;

    @Schema(description = "路由路径(浏览器地址栏路径)")
    @NotBlank(message = "路由路径不能为空",groups = {Catalog.class,Menu.class,ExitLink.class})
    private String path;

    @Schema(description = "组件路径(vue页面完整路径，省略.vue后缀)")
    @NotNull(message = "组件路径不能为空",groups = {Menu.class})
    //todo exitlink 当 path 不是 http 开头的，就是必填项
    // @NotNull(message = "组件路径不能为空",groups = {ExitLink.class})
    private String component;

    @Schema(description = "权限标识")
    @NotNull(message = "权限标识不能为空",groups = {Button.class})
    private String perm;

    @Schema(description = "显示状态(true-显示;false-隐藏)")
    @NotNull(message = "显示状态不能为空",groups = {Catalog.class,Menu.class,ExitLink.class})
    private Boolean visible;

    @Schema(description = "排序")
    @NotNull(message = "排序不能为空")
    private Integer sort;

    @Schema(description = "菜单图标")
    @NotBlank(message = "菜单图标不能为空",groups = {Catalog.class,Menu.class,ExitLink.class})
    private String icon;

    @Schema(description = "跳转路径")
    @NotBlank(message = "跳转路径不能为空",groups = {Catalog.class})
    private String redirect;

    @Schema(description = "【目录】只有一个子路由是否始终显示(1:是 0:否)")
    @NotNull(message = "目录不能为空",groups = {Catalog.class})
    private Byte alwaysShow;

    @Schema(description = "【菜单】是否开启页面缓存(1:是 0:否)")
    @NotNull(message = "菜单不能为空",groups = {Menu.class})
    private Byte keepAlive;

    @Schema(description = "操作类型（后台校验规则使用），true 更新操作，false 保存操作")
    @NotNull(message = "操作类型不能为空")
    private Boolean isUpdateHandle;

    public interface ExitLink{}
    public interface Button{}
    public interface Menu{}
    public interface Catalog{}
}
