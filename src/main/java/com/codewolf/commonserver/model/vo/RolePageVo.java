package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @ClassName RolePageVo
 * @Description 角色分页视图实体
 * @Author code wolf
 * @Date 2024/1/15 11:17
 * @Version 1.0
 */
@Schema(name="角色分页视图实体")
@Data
public class RolePageVo {

    @Schema(description = "角色id")
    private Long id;

    @Schema(description = "角色名称")
    private String name;

    @Schema(description = "角色编码")
    private String code;

    @Schema(description = "显示顺序")
    private Integer sort;

    @Schema(description = "角色状态(1-正常；0-停用)")
    private Boolean status;

    @Schema(description = "数据权限(0-所有数据；1-部门及子部门数据；2-本部门数据；3-本人数据)")
    private Byte dataScope;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}
