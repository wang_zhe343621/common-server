package com.codewolf.commonserver.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @ClassName TreeNodeVo
 * @Description 树形节点
 * @Author code wolf
 * @Date 2024/1/20 23:59
 * @Version 1.0
 */
@Schema(name = "树形结构实体")
@Data
public class TreeNodeVo<T> {
    private T data;
    private List<TreeNodeVo<T>> children;

    public void addChild(TreeNodeVo<T> child) {
        this.children.add(child);
    }

    public void removeChild(TreeNodeVo<T> child) {
        this.children.remove(child);
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }
}
