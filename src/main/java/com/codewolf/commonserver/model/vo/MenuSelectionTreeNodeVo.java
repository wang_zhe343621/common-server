package com.codewolf.commonserver.model.vo;

import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName MenuSelectionTreeNodeVo
 * @Description 菜单下拉选择树形节点视图实体
 * @Author code wolf
 * @Date 2024/1/24 22:47
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Schema(name = "菜单下拉选择树形节点视图实体")
@Data
public class MenuSelectionTreeNodeVo extends TreeNodeBase {
    @Schema(name = "菜单id")
    private Long id;

    @Schema(name = "菜单名称")
    private String name;

    @Schema(name = "排序")
    private Integer sort;

}
