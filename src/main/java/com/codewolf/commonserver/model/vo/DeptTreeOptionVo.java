package com.codewolf.commonserver.model.vo;

import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName DeptTreeOptionVo
 * @Description 部门树形下拉选择视图对象
 * @Author code wolf
 * @Date 2024/1/23 22:42
 * @Version 1.0
 */
@Schema(name="部门树形下拉选择视图对象")
@Data
public class DeptTreeOptionVo extends TreeNodeBase {
    @Schema(name="部门id")
    private Long id;

    @Schema(name="部门名称")
    private String name;

    @Schema(name = "排序")
    private Integer sort;

}
