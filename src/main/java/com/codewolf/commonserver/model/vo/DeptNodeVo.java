package com.codewolf.commonserver.model.vo;

import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @ClassName DeptListVo
 * @Description 部门列表视图对象
 * @Author code wolf
 * @Date 2024/1/22 16:44
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Schema(name="部门列表")
@Data
public class DeptNodeVo extends TreeNodeBase {

    @Schema(description = "部门id")
    private Long id;

    @Schema(description = "部门名称")
    private String name;

    @Schema(description = "显示顺序")
    private Integer sort;

    @Schema(description = "状态(1:正常;0:禁用)")
    private Byte status;




}
