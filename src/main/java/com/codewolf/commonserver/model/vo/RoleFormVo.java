package com.codewolf.commonserver.model.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName RoleFormVo
 * @Description 角色表单实体
 * @Author code wolf
 * @Date 2024/1/16 16:33
 * @Version 1.0
 */
@Schema(name="角色表单实体")
@Data
public class RoleFormVo {
    @Schema(description = "角色id")
    private Long id;

    @Schema(description = "角色名称")
    private String name;

    @Schema(description = "角色编码")
    private String code;

    @Schema(description = "显示顺序")
    private Integer sort;

    @Schema(description = "角色状态(1-正常；0-停用)")
    private Boolean status;

    @Schema(description = "数据权限(0-所有数据；1-部门及子部门数据；2-本部门数据；3-本人数据)")
    private Byte dataScope;
}
