package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

/**
 * @ClassName UserInfoVo
 * @Description 当前登录用户视图对象
 * @Author code wolf
 * @Date 2024/1/4 12:32
 * @Version 1.0
 */
@Schema(description = "当前登录用户视图对象")
@Data
public class UserInfoVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "用户id")
    private Integer userId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "昵称")
    @TableField("nickname")
    private String nickname;

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "用户角色编码集合")
    private Set<String> roles;

    @Schema(description = "用户权限编码集合")
    private Set<String> perms;
}
