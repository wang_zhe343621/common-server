package com.codewolf.commonserver.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.codewolf.commonserver.model.common.TreeNodeBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @ClassName MenuTreeNodeVo
 * @Description 菜单树形节点视图实体
 * @Author code wolf
 * @Date 2024/1/24 19:48
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Schema(name = "菜单树形节点视图实体")
@Data
public class MenuTreeNodeVo extends TreeNodeBase {

    @Schema(description = "菜单Id")
    private Long id;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "菜单名称")
    private String name;

    @Schema(description = "菜单类型(1:菜单 2:目录 3:外链 4:按钮)")
    private Byte type;

    @Schema(description = "路由路径(浏览器地址栏路径)")
    private String path;

    @Schema(description = "组件路径(vue页面完整路径，省略.vue后缀)")
    private String component;

    @Schema(description = "权限标识")
    private String perm;

    @Schema(description = "显示状态(1-显示;0-隐藏)")
    private Boolean visible;

    @Schema(description = "排序")
    private Integer sort;

}
