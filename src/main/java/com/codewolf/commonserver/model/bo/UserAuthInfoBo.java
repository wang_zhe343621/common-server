package com.codewolf.commonserver.model.bo;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

/**
 * @ClassName UserAuthInfoBo
 * @Description 用户认证信息
 * @Author code wolf
 * @Date 2024/1/1 21:25
 * @Version 1.0
 */
@Data
public class UserAuthInfoBo implements Serializable {

    @Serial
    private static final long serialVersionUID = 3246449533381653192L;

    private Long userId;
    private String username;
    private String password;
    private Long deptId;
    private Integer dataScope;
    private Boolean enabled;
    private Set<String> roles;
    private Set<String> perms;

}
