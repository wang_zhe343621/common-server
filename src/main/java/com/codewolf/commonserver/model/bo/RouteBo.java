package com.codewolf.commonserver.model.bo;

import com.codewolf.commonserver.model.common.MenuBase;
import lombok.Data;

/**
 * @ClassName RouteBo
 * @Description 路由bo
 * @Author code wolf
 * @Date 2024/1/8 0:36
 * @Version 1.0
 */
@Data
public class RouteBo implements MenuBase {
    private Long id;

    private Long parentId;

    private String name;

    private Byte type;

    private String path;

    private String component;

    private String perm;

    private Integer sort;

    private String icon;

    private String redirect;

    private Byte alwaysShow;

    private Byte keepAlive;
    private Boolean visible;

    @Override
    public Long getMenuParentId() {
        return this.parentId;
    }

    @Override
    public Long getMenuId() {
        return this.id;
    }
}
