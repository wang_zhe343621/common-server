package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;

import com.codewolf.commonserver.model.common.MenuBase;
import com.codewolf.commonserver.model.common.TreeNodeOriginBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 菜单管理
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_menu")
@Schema(name = "SysMenu", description = "菜单管理")
public class SysMenu extends BaseEntity implements MenuBase, TreeNodeOriginBase {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "父菜单ID")
    @TableField("parent_id")
    private Long parentId;

    @Schema(description = "父节点ID路径")
    @TableField("tree_path")
    private String treePath;

    @Schema(description = "菜单名称")
    @TableField("name")
    private String name;

    @Schema(description = "菜单类型(1:菜单 2:目录 3:外链 4:按钮)")
    @TableField("type")
    private Byte type;

    @Schema(description = "路由路径(浏览器地址栏路径)")
    @TableField("path")
    private String path;

    @Schema(description = "组件路径(vue页面完整路径，省略.vue后缀)")
    @TableField("component")
    private String component;

    @Schema(description = "权限标识")
    @TableField("perm")
    private String perm;

    @Schema(description = "显示状态(1-显示;0-隐藏)")
    @TableField("visible")
    private Boolean visible;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "菜单图标")
    @TableField("icon")
    private String icon;

    @Schema(description = "跳转路径")
    @TableField("redirect")
    private String redirect;

    @Schema(description = "【目录】只有一个子路由是否始终显示(1:是 0:否)")
    @TableField("always_show")
    private Byte alwaysShow;

    @Schema(description = "【菜单】是否开启页面缓存(1:是 0:否)")
    @TableField("keep_alive")
    private Byte keepAlive;

    @Override
    public Long getMenuParentId() {
        return (this.parentId);
    }

    @Override
    public Long getMenuId() {
        return this.id;
    }

    @Override
    public Long getTreeNodeParentId() {
        return this.parentId;
    }

    @Override
    public Long getTreeNodeId() {
        return this.id;
    }
}
