package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;
import java.io.Serializable;

import com.codewolf.commonserver.model.common.TreeNodeOriginBase;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dept")
@Schema(name = "SysDept", description = "部门表")
public class SysDept extends BaseEntity implements TreeNodeOriginBase {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "部门名称")
    @TableField("name")
    private String name;

    @Schema(description = "父节点id")
    @TableField("parent_id")
    private Long parentId;

    @Schema(description = "父节点id路径")
    @TableField("tree_path")
    private String treePath;

    @Schema(description = "显示顺序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "状态(1:正常;0:禁用)")
    @TableField("status")
    private Byte status;

    @Schema(description = "逻辑删除标识(1:已删除;0:未删除)")
    @TableField("deleted")
    @TableLogic
    private Byte deleted;

    @Schema(description = "创建人ID")
    @TableField("create_by")
    private Long createBy;

    @Schema(description = "修改人ID")
    @TableField("update_by")
    private Long updateBy;

    @Override
    public Long getTreeNodeParentId() {
        return this.parentId;
    }

    @Override
    public Long getTreeNodeId() {
        return this.id;
    }
}
