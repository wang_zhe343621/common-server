package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典数据表
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dict")
@Schema(name = "SysDict", description = "字典数据表")
public class SysDict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "字典类型编码")
    @TableField("type_code")
    private String typeCode;

    @Schema(description = "字典项名称")
    @TableField("name")
    private String name;

    @Schema(description = "字典项值")
    @TableField("value")
    private String value;

    @Schema(description = "排序")
    @TableField("sort")
    private Integer sort;

    @Schema(description = "状态(1:正常;0:禁用)")
    @TableField("status")
    private Byte status;

    @Schema(description = "是否默认(1:是;0:否)")
    @TableField("defaulted")
    private Byte defaulted;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;
}
