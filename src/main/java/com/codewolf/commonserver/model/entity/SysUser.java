package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_user")
@Schema(name = "SysUser", description = "用户信息表")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "用户名")
    @TableField("username")
    private String username;

    @Schema(description = "昵称")
    @TableField("nickname")
    private String nickname;

    @Schema(description = "性别((1:男;2:女))")
    @TableField("gender")
    private Boolean gender;

    @Schema(description = "密码")
    @TableField("password")
    private String password;

    @Schema(description = "部门ID")
    @TableField("dept_id")
    private Integer deptId;

    @Schema(description = "用户头像")
    @TableField("avatar")
    private String avatar;

    @Schema(description = "联系方式")
    @TableField("mobile")
    private String mobile;

    @Schema(description = "用户状态((1:正常;0:禁用))")
    @TableField("status")
    private Boolean status;

    @Schema(description = "用户邮箱")
    @TableField("email")
    private String email;

    @Schema(description = "逻辑删除标识(0:未删除;1:已删除)")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;
}
