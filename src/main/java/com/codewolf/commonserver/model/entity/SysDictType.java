package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 字典类型表
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dict_type")
@Schema(name = "SysDictType", description = "字典类型表")
public class SysDictType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Schema(description = "主键 ")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @Schema(description = "类型名称")
    @TableField("name")
    private String name;

    @Schema(description = "类型编码")
    @TableField("code")
    private String code;

    @Schema(description = "状态(0:正常;1:禁用)")
    @TableField("status")
    private Boolean status;

    @Schema(description = "备注")
    @TableField("remark")
    private String remark;
}
