package com.codewolf.commonserver.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codewolf.commonserver.common.base.BaseEntity;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色和菜单关联表
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_role_menu")
@Schema(name = "SysRoleMenu", description = "角色和菜单关联表")
@Builder
public class SysRoleMenu  {

    private static final long serialVersionUID = 1L;

    @Schema(description = "角色ID")
    @TableField("role_id")
    private Long roleId;

    @Schema(description = "菜单ID")
    @TableField("menu_id")
    private Long menuId;
}
