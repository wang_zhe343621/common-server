package com.codewolf.commonserver.model.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @ClassName TreeNode
 * @Description 树结构节点基类
 * @Author code wolf
 * @Date 2024/1/20 23:47
 * @Version 1.0
 */

@Data
public class TreeNodeBase {

    private List<? extends TreeNodeBase> children;

}
