package com.codewolf.commonserver.model.common;

/**
 * @ClassName MenuTreeItemBase
 * @Description 菜单树方法基类
 * @Author code wolf
 * @Date 2024/1/20 21:58
 * @Version 1.0
 */
public interface MenuBase {

    /**
     * 获取菜单父节点id
     */
    Long getMenuParentId();

    /**
     * 获取菜单Id
     */
    Long getMenuId();

}
