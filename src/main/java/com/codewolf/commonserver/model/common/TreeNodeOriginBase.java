package com.codewolf.commonserver.model.common;

/**
 * @ClassName TreeNodeOrignBase
 * @Description 树节点源数据基类
 * @Author code wolf
 * @Date 2024/1/22 21:14
 * @Version 1.0
 */
public interface TreeNodeOriginBase {
    /**
     * 获取父节点id
     */
    Long getTreeNodeParentId();

    /**
     * 获取节点Id
     */
    Long getTreeNodeId();
}
