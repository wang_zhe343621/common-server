package com.codewolf.commonserver.common.validator.groups;

import cn.hutool.core.util.ObjUtil;
import com.codewolf.commonserver.model.vo.MenuFormVo;
import jakarta.validation.groups.Default;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MenuFormVoGroupSequenceProvider
 * @Description MenuFormVo 分组选择器
 * @Author code wolf
 * @Date 2024/1/25 0:13
 * @Version 1.0
 */
public class MenuFormVoGroupSequenceProvider implements DefaultGroupSequenceProvider<MenuFormVo> {

    @Override
    public List<Class<?>> getValidationGroups(MenuFormVo menuFormVo) {
        ArrayList<Class<?>> validationGroups = new ArrayList<>();
        //默认分组
        validationGroups.add(MenuFormVo.class);
        if (menuFormVo == null) {
            return validationGroups;
        }
        //update or save 分组
        if (ObjUtil.isNotNull(menuFormVo.getIsUpdateHandle())) {
            if (menuFormVo.getIsUpdateHandle()) {
                validationGroups.add(Update.class);
            } else {
                validationGroups.add(Save.class);
            }
        }

        //type 分组
        if (ObjUtil.isNotNull(menuFormVo.getType())) {
            switch (menuFormVo.getType()) {
                case 1 -> validationGroups.add(MenuFormVo.Catalog.class);
                case 2 -> validationGroups.add(MenuFormVo.Menu.class);
                case 3 -> validationGroups.add(MenuFormVo.Button.class);
                case 4 -> validationGroups.add(MenuFormVo.ExitLink.class);
            }
        }

        return validationGroups;
    }

}