package com.codewolf.commonserver.common.base;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName BasePageQuery
 * @Description 分页请求基类
 * @Author code wolf
 * @Date 2023/12/30 0:21
 * @Version 1.0
 */
@Data
@Schema(description = "分页请求基类")
public class BasePageQuery {

    @Schema(description = "页码", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    private int pageNum = 1;
    @Schema(description = "每页记录数", example = "10", requiredMode = Schema.RequiredMode.REQUIRED)
    private int pageSize = 10;

}
