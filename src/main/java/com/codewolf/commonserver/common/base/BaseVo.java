package com.codewolf.commonserver.common.base;

import java.io.Serial;
import java.io.Serializable;
import java.lang.invoke.SerializedLambda;

/**
 * @ClassName BaseVo
 * @Description 视图对象基类
 * @Author code wolf
 * @Date 2023/12/30 0:19
 * @Version 1.0
 */
public class BaseVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

}
