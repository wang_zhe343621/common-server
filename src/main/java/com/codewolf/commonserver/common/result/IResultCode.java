package com.codewolf.commonserver.common.result;

public interface IResultCode {
    String getCode();

    String getMsg();

}
