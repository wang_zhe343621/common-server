package com.codewolf.commonserver.common.result;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import static com.codewolf.commonserver.common.result.ResultCode.*;


/**
 * @author WangZhe
 * @version 1.0
 * @className Result
 * @description 统一返回结果
 * @date 2022/8/3 10:32
 */

@Data
@Schema(name = "Result", description = "统一返回结果")
public class Result<T> {

    /**
     * 返回状态
     */
    @Schema(description = "返回状态")
    private Boolean flag;
    /**
     * 返回码
     */
    @Schema(description = "返回状态")
    private String code;
    /**
     * 返回信息
     */
    @Schema(description = "返回信息")
    private String message;
    /**
     * 返回数据
     */
    @Schema(description = "返回数据")
    private T data;

    public static <T> Result<T> ok() {
        return restResult(true, null, SUCCESS.getCode(), SUCCESS.getMsg());
    }

    public static <T> Result<T> ok(T data) {
        return restResult(true, data, SUCCESS.getCode(), SUCCESS.getMsg());
    }

    public static <T> Result<T> ok(T data, String message) {
        return restResult(true, data, SUCCESS.getCode(), message);
    }

    public static <T> Result<T> fail() {
        return restResult(false, null, FAIL.getCode(), FAIL.getMsg());
    }

    public static <T> Result<T> fail(IResultCode resultCode) {
        return restResult(false, null, resultCode.getCode(), resultCode.getMsg());
    }

    public static <T> Result<T> fail(IResultCode resultCode, T data) {
        return restResult(false, data, resultCode.getCode(), resultCode.getMsg());
    }
    public static <T> Result<T> fail(IResultCode resultCode, String msg) {

        return restResult(false, null, resultCode.getCode(), msg);
    }
    //todo 添加返回对象的时间转换

    public static <T> Result<T> fail(String message) {
        return restResult(false, message);
    }

    public static <T> Result<T> fail(T data) {
        return restResult(false, data, FAIL.getCode(), FAIL.getMsg());
    }

    public static <T> Result<T> fail(T data, String message) {
        return restResult(false, data, FAIL.getCode(), message);
    }

    public static <T> Result<T> fail(String code, String message) {
        return restResult(false, null, code, message);
    }

    private static <T> Result<T> restResult(Boolean flag, String message) {
        Result<T> apiResult = new Result<>();
        apiResult.setFlag(flag);
        apiResult.setCode(flag ? SUCCESS.getCode() : FAIL.getCode());
        apiResult.setMessage(message);
        return apiResult;
    }

    private static <T> Result<T> restResult(Boolean flag, T data, String code, String message) {
        Result<T> apiResult = new Result<>();
        apiResult.setFlag(flag);
        apiResult.setData(data);
        apiResult.setCode(code);
        apiResult.setMessage(message);
        return apiResult;
    }

}
