package com.codewolf.commonserver.common.util;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.Collections;


/**
 * @ClassName MybatisPlusGenerator
 * @Description mybatisPlus 代码生成器
 * @Author code wolf
 * @Date 2023/12/14 12:15
 * @Version 1.0
 */
public class MybatisPlusGenerator {
    private static final String URL = "jdbc:mysql://localhost:3306/common_server";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    private static final String BASE_DIR = System.getProperty("user.dir");

    public static void main(String[] args) {
        ArrayList<String> tableName = new ArrayList<>();
        tableName.add("sys_user_role");
        tableName.add("sys_user");
        tableName.add("sys_role_menu");
        tableName.add("sys_role");
        tableName.add("sys_menu");
        tableName.add("sys_dict_type");
        tableName.add("sys_dict");
        tableName.add("sys_dept");

        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                .globalConfig(builder -> {
                    builder.author("code wolf") // 设置作者
                            .enableSpringdoc()
                            .commentDate("yyyy-mm-dd")
                            .outputDir(BASE_DIR + "\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.codewolf.commonserver") // 设置父包名
//                            .moduleName("blog") //会显示在路径中
                            .entity("model.entity")
                            .service("service")
                            .serviceImpl("service.impl")
                            .mapper("mapper")
                            .xml("mapper")
                            .pathInfo(Collections.singletonMap(OutputFile.xml, BASE_DIR + "\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
//                .templateConfig(builder -> {
//                    builder.entity("/templates/entity.java");
//                })
                .strategyConfig(builder -> {
                    builder.addInclude(tableName) // 设置需要生成的表名
                            .addTablePrefix("tb_", "c_") // 设置过滤表前缀
                            //controller config
                            .controllerBuilder().enableFileOverride().formatFileName("%sController")
                            .enableRestStyle()
                            //service config
                            .serviceBuilder().enableFileOverride().formatServiceFileName("%sService").formatServiceImplFileName("%sServiceImpl")
                            //entity config
                            .entityBuilder().enableFileOverride()
                            .enableLombok()
                            .logicDeleteColumnName("deleted")
                            .enableTableFieldAnnotation()
                            .superClass("com.codewolf.commonserver.common.base.BaseEntity")
                            .enableChainModel()
                            .addSuperEntityColumns("create_time", "update_time")
                            //mapper xml config
                            .mapperBuilder().enableFileOverride().formatMapperFileName("%sMapper")
                            .enableBaseResultMap()
                            .mapperAnnotation(Mapper.class)
                            .enableBaseColumnList()
                            .superClass(BaseMapper.class);
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
