package com.codewolf.commonserver.common.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjUtil;
import com.codewolf.commonserver.model.common.TreeNodeBase;
import com.codewolf.commonserver.model.common.TreeNodeOriginBase;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CommonUtil
 * @Description 公共工具类
 * @Author code wolf
 * @Date 2024/1/22 21:06
 * @Version 1.0
 */
public class CommonUtil {
    public static <T extends TreeNodeOriginBase,K extends TreeNodeBase> List<K> buildTree(Long parentId, List<T> dataList, TreeNodeConvert<T,K> treeNodeConvert, Comparator<K> comparator) {
        List<K> result = dataList.stream()
                .filter(item -> item.getTreeNodeParentId().equals(parentId))
                .map(item -> {
                    List<K> children = buildTree(item.getTreeNodeId(), dataList, treeNodeConvert, comparator);
                    K treeNode = treeNodeConvert.convert(item);
                    treeNode.setChildren(children);
                    return treeNode;
                }).sorted(comparator).collect(Collectors.toList());

        return result;
    }

    public interface TreeNodeConvert<T,K> {
        K convert(T source);
    }
}
