package com.codewolf.commonserver.common.util;

import cn.hutool.core.util.StrUtil;
import com.codewolf.commonserver.common.constant.SystemConstant;
import com.codewolf.commonserver.common.exception.BusinessException;
import com.codewolf.commonserver.common.result.ResultCode;
import com.codewolf.commonserver.core.security.model.SysUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @ClassName SecurityUtil
 * @Description security 工具类
 * @Author code wolf
 * @Date 2024/1/4 12:37
 * @Version 1.0
 */
public class SecurityUtil {

    /**
     * 获取当前登录人信息
     */
    public static SysUserDetails getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        return (SysUserDetails) principal;
    }

    /**
     * 获取用户id
     */
    public static Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 获取用户role
     */
    public static Set<String> getRoles() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return authorities.stream().map(item -> StrUtil.removePrefix(item.getAuthority(), "ROLE_")).collect(Collectors.toSet());
    }

    /**
     * 是否是超级管理员
     */
    public static boolean isRoot() {
        Set<String> roles = getRoles();
        return roles.contains(SystemConstant.ROOT_ROLE_CODE);
    }

}
