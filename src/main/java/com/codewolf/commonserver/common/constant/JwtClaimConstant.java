package com.codewolf.commonserver.common.constant;

/**
 * @ClassName JwtClaimConstant
 * @Description jwt claim 中的用户信息
 * @Author code wolf
 * @Date 2023/12/30 0:36
 * @Version 1.0
 */
public interface JwtClaimConstant {
    /**
     * 用户id
     */
    String USER_ID = "userId";

    /**
     * 用户名
     */
    String USERNAME = "username";

    /**
     * 数据权限
     */
    String DATA_SCOPE = "dataScope";

    /**
     * 权限(角色Code)集合
     */
    String AUTHORITIES = "authorities";

    String DEPT_ID="deptId";

}
