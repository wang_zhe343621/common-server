package com.codewolf.commonserver.common.constant;

/**
 * @ClassName SecurityConstant
 * @Description security 常量
 * @Author code wolf
 * @Date 2023/12/30 0:35
 * @Version 1.0
 */
public interface SecurityConstant {

    /**
     * 登录路径
     */
    String LOGIN_PATH = "/api/v1/auth/login";
}
