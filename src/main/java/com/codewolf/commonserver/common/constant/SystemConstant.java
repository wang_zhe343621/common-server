package com.codewolf.commonserver.common.constant;

/**
 * @ClassName SystemConstant
 * @Description 系统常量
 * @Author code wolf
 * @Date 2024/1/9 13:25
 * @Version 1.0
 */
public interface SystemConstant {
    /**
     * 跟路由id
     */
    Long ROOT_ID = 0L;

    /**
     * 超级管理员角色编码
     */
    String ROOT_ROLE_CODE = "ROOT";

    /**
     * 顶级部门id
     */
    Long DEPT_ROOT_ID = 0L;

    /**
     * 顶级菜单id
     */
    Long MENU_ROOT_ID = 0L;
}
