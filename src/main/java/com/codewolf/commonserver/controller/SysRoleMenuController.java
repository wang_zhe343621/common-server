package com.codewolf.commonserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色和菜单关联表 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@RestController
@RequestMapping("/sysRoleMenu")
public class SysRoleMenuController {

}
