package com.codewolf.commonserver.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.codewolf.commonserver.common.result.Result;
import com.codewolf.commonserver.model.param.RoleInsertVo;
import com.codewolf.commonserver.model.param.RoleQueryPageVo;
import com.codewolf.commonserver.model.param.RoleUpdateVo;
import com.codewolf.commonserver.model.vo.RoleFormVo;
import com.codewolf.commonserver.model.vo.RolePageVo;
import com.codewolf.commonserver.service.SysRoleMenuService;
import com.codewolf.commonserver.service.SysRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Tag(name = "角色控制器")
@RestController
@RequestMapping("/api/v1/role")
@RequiredArgsConstructor
@Validated
public class SysRoleController {
    private final SysRoleService roleService;
    private final SysRoleMenuService roleMenuService;

    @Operation(summary = "角色分页列表")
    @GetMapping("/roles/page")
    public Result<Page<RolePageVo>> getRolesPage(@ParameterObject RoleQueryPageVo roleQueryPageVo) {
        Page<RolePageVo> rolesPage = roleService.getRolesPage(roleQueryPageVo.getKeyword(), roleQueryPageVo.getPageSize(), roleQueryPageVo.getPageNum());
        return Result.ok(rolesPage);
    }

    @Operation(summary = "删除角色")
    @DeleteMapping("/{ids}")
    @Parameter(name = "ids", description = "角色id,以英文,分隔", in = ParameterIn.PATH)
    public Result<?> deleteRoles(@PathVariable @NotBlank(message = "不馁为空") String ids) {
        roleService.deleteRoles(ids);
        return Result.ok();
    }


    @Operation(summary = "返回角色表单数据")
    @GetMapping("/{roleId}/form")
    @Parameter(name = "id", description = "角色id", in = ParameterIn.PATH)
    public Result<RoleFormVo> getRoleForm(@PathVariable @NotNull(message = "角色id不能为空") Long roleId) {
        RoleFormVo roleForm = roleService.getRoleForm(roleId);
        return Result.ok(roleForm);
    }

    @Operation(summary = "更新角色")
    @PutMapping("/{roleId}")
    @Validated
    @Parameter(name = "id", description = "角色id", in = ParameterIn.PATH)
    public Result<?> updateRole(@PathVariable @NotNull(message = "角色id不能为空") Long roleId, @RequestBody RoleUpdateVo roleUpdateVo) {
        roleService.updateRole(roleUpdateVo);
        return Result.ok();
    }

    @Operation(summary = "添加角色")
    @PostMapping
    @Validated
    public Result<?> addRole(@RequestBody RoleInsertVo roleInsertVo) {
        roleService.insertRole(roleInsertVo);
        return Result.ok();
    }

    @Operation(summary = "角色拥有的菜单id集合")
    @GetMapping("/{roleId}/menuIds")
    @Validated
    @Parameter(name = "roleId", description = "角色id", in = ParameterIn.PATH)
    public Result<List<Long>> getRoleMenuIds(@PathVariable @NotNull(message = "角色id不能为空") Long roleId) {
        List<Long> roleMenuIds = roleService.getRoleMenuIds(roleId);
        return Result.ok(roleMenuIds);
    }

    @Operation(summary = "分配菜单给角色")
    @PutMapping("/{roleId}/menus")
    @Validated
    @Parameter(name = "roleId", description = "角色id", in = ParameterIn.PATH)
    public Result<?> updateRoleMenus(
            @PathVariable @NotNull(message = "角色id不能为空") Long roleId,
            @RequestBody List<Long> menuIds) {
        roleMenuService.updateRoleMenus(roleId, menuIds);
        return Result.ok();
    }


}
