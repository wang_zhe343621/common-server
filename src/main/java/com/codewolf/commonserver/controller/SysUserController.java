package com.codewolf.commonserver.controller;

import com.codewolf.commonserver.common.result.Result;
import com.codewolf.commonserver.model.vo.UserInfoVo;
import com.codewolf.commonserver.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Tag(name = "用户信息控制器")
@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class SysUserController {
    private final SysUserService userService;

    @GetMapping("/me")
    @Operation(summary = "获取当前登录用户信息")
    public Result<UserInfoVo> getCurrentUserInfo() {
        UserInfoVo currentUserInfo = userService.getCurrentUserInfo();
        return Result.ok(currentUserInfo);
    }

}
