package com.codewolf.commonserver.controller;

import com.codewolf.commonserver.common.result.Result;
import com.codewolf.commonserver.common.validator.groups.Save;
import com.codewolf.commonserver.common.validator.groups.Update;
import com.codewolf.commonserver.model.param.DeptQueryVo;
import com.codewolf.commonserver.model.vo.DeptFormVo;
import com.codewolf.commonserver.model.vo.DeptNodeVo;
import com.codewolf.commonserver.model.vo.DeptTreeOptionVo;
import com.codewolf.commonserver.service.SysDeptService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Tag(name = "部门控制器")
@RestController
@RequestMapping("/api/v1/dept")
@RequiredArgsConstructor
@Validated
public class SysDeptController {

    private final SysDeptService deptService;


    @Operation(summary = "获取部门列表")
    @GetMapping("/treeList")
    public Result<List<DeptNodeVo>> getDeptList(@ParameterObject DeptQueryVo deptQueryVo) {
        List<DeptNodeVo> deptList = deptService.getDeptList(deptQueryVo.getKeyword(), deptQueryVo.getDeptStatus());
        return Result.ok(deptList);
    }

    @Operation(summary = "获取部门树形选择器配置数据")
    @GetMapping("/treeSelectOption")
    public Result<List<DeptTreeOptionVo>> getDeptTreeSelectOption() {
        List<DeptTreeOptionVo> deptTreeSelectOption = deptService.getDeptTreeSelectOption();
        return Result.ok(deptTreeSelectOption);
    }

    @Operation(summary = "获取部门表单数据")
    @GetMapping("/{deptId}/form")
    public Result<?> getDeptForm(@NotNull(message = "部门id不能为空")
                                 @Min(value = 0, message = "部门id是数字且最小为0") @PathVariable Long deptId) {
        DeptFormVo deptForm = deptService.getDeptForm(deptId);
        return Result.ok(deptForm);
    }

    @Operation(summary = "新增部门")
    @PostMapping
    public Result<?> insertDept(@Validated(Save.class) @RequestBody DeptFormVo deptFormVo) {
        deptService.insertDept(deptFormVo);
        return Result.ok();
    }

    @Operation(summary = "更新部门")
    @PutMapping
    public Result<?> updateDept(@Validated(Update.class) @RequestBody DeptFormVo deptFormVo) {
        deptService.updateDept(deptFormVo);
        return Result.ok();
    }

    @Operation(summary = "删除部门")
    @DeleteMapping
    @Parameter(name = "ids", description = "部门id集合（以英文逗号分隔）")
    public Result<?> deleteDepts(
            @NotBlank(message = "部门id不能为空")
            @Pattern(regexp = "^\\d+(,\\d+)*$", message = "参数不规范") String ids) {
        deptService.deleteDepts(ids);
        return Result.ok();
    }

}
