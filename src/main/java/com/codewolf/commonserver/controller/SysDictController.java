package com.codewolf.commonserver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 字典数据表 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@RestController
@RequestMapping("/sysDict")
public class SysDictController {

}
