package com.codewolf.commonserver.controller;

import com.codewolf.commonserver.common.result.Result;
import com.codewolf.commonserver.common.validator.groups.Save;
import com.codewolf.commonserver.common.validator.groups.Update;
import com.codewolf.commonserver.model.common.MenuBase;
import com.codewolf.commonserver.model.vo.*;
import com.codewolf.commonserver.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.groups.Default;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Tag(name = "菜单控制器")
@RestController
@RequestMapping("/api/v1/menu")
@RequiredArgsConstructor
@Validated
public class SysMenuController {

    private final SysMenuService menuService;

    @Operation(summary = "路由列表")
    @GetMapping("/listRoutes")
    public Result<?> listRoutes() {
        List<RouteVo> routeVoList = menuService.listRoutes();
        return Result.ok(routeVoList);
    }

    @Operation(summary = "菜单配置")
    @GetMapping("/option")
    public Result<List<MenuOptionVo>> getMenuOption() {
        List<MenuOptionVo> menuOption = menuService.getMenuOption();
        return Result.ok(menuOption);
    }

    @Operation(summary = "获取菜单树形列表")
    @GetMapping("/treeList")
    @Parameter(name = "keyword", description = "菜单名称", in = ParameterIn.QUERY)
    public Result<List<MenuTreeNodeVo>> getTreeList(@RequestParam(value = "keyword", required = false) String keyword) {
        List<MenuTreeNodeVo> treeList = menuService.getTreeList(keyword);
        return Result.ok(treeList);
    }


    @Operation(summary = "获取菜单下拉选项树形列表")
    @GetMapping("/selectionTreeList")
    public Result<List<MenuSelectionTreeNodeVo>> getMenuSelectionTreeList() {
        List<MenuSelectionTreeNodeVo> selectionTreeList = menuService.getSelectionTreeList();
        return Result.ok(selectionTreeList);
    }

    @Operation(summary = "新增菜单")
    @PostMapping
    public Result<?> saveMenu(@Validated @RequestBody MenuFormVo menuFormVo) {
        menuService.addMenu(menuFormVo);
        return Result.ok();
    }

    @Operation(summary = "更新菜单")
    @PutMapping
    public Result<?> updateMenu(@Validated @RequestBody MenuFormVo menuFormVo) {
        menuService.updateMenu(menuFormVo);
        return Result.ok();
    }

    @Operation(summary = "删除菜单")
    @DeleteMapping("/{menuId}")
    public Result<?> deleteMenu(@NotNull(message = "菜单Id 不能为空") @PathVariable Long menuId) {
        menuService.deleteMenu(menuId);
        return Result.ok();
    }

    @Operation(summary = "菜单表单数据")
    @GetMapping("/{menuId}/form")
    public Result<MenuFormVo> getMenuForm(@NotNull(message = "菜单id不能为空") @PathVariable Long menuId) {
        MenuFormVo menuForm = menuService.getMenuForm(menuId);
        return Result.ok(menuForm);
    }


}
