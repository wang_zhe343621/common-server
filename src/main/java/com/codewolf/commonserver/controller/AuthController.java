package com.codewolf.commonserver.controller;

import com.codewolf.commonserver.common.result.Result;
import com.codewolf.commonserver.model.dto.LoginResult;
import com.codewolf.commonserver.service.AuthService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName AuthController
 * @Description 认证控制器
 * @Author code wolf
 * @Date 2023/12/31 12:24
 * @Version 1.0
 */
@Tag(name = "认证控制器")
@RequestMapping("/api/v1/auth")
@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/login")
    public Result<LoginResult> login(@RequestParam String username, @RequestParam String password) {
        LoginResult loginResult = authService.login(username, password);
        return Result.ok(loginResult);
    }
}
