package com.codewolf.commonserver.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.model.ClassGeneratingPropertyAccessorFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName IndexController
 * @Description 测试文件
 * @Author code wolf
 * @Date 2023/12/14 11:31
 * @Version 1.0
 */
@RestController
@Tag(name = "测试控制器")
public class IndexController {


    @GetMapping("/test")
    @Operation(summary = "普通body请求")
    public String index() {
        return "123";
    }

    @PostMapping("/spring")
    public String test() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("spring");
        System.out.println("authentication");

        return "redirect:spring";
    }
/*    @Operation(summary = "普通body请求+Param+Header+Path")
    @Parameters({
            @Parameter(name = "id",description = "文件id",in = ParameterIn.PATH),
            @Parameter(name = "token",description = "请求token",required = true,in = ParameterIn.HEADER),
            @Parameter(name = "name",description = "文件名称",required = true,in=ParameterIn.QUERY)
    })
    @ApiResponse(responseCode = "404", description = "foo")
    @PostMapping("/body/{id}")
    public String bodyParamHeaderPath(@PathVariable("id") String id, @RequestHeader("token") String token, @RequestParam("name")String name, @RequestBody User user){
        return "password:"+ user.getPassword()+",receiveName:"+name+",token:"+token+",pathID:"+id;
    }*/
}
