package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
