package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.bo.RouteBo;
import com.codewolf.commonserver.model.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    Set<String> getPermsListByRoles(Set<String> roles);

    List<RouteBo> getListRoutes(Long userId);
}
