package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.bo.UserAuthInfoBo;
import com.codewolf.commonserver.model.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return 用户认证信息
     */
    UserAuthInfoBo getUserAuthInfo(String username);
}
