package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 统计角色绑定的用户数量
     * @param roleId 角色id
     */
    int countUsersForRole(Long roleId);
}
