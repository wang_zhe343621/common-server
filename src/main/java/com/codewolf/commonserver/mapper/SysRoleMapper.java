package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 获取角色菜单id集合
     * @param roleId 角色id
     */
   List<Long> getRoleMenuIds(Long roleId);
}
