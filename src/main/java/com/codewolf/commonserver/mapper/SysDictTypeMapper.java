package com.codewolf.commonserver.mapper;

import com.codewolf.commonserver.model.entity.SysDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author code wolf
 * @since 2023-20-29
 */
@Mapper
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {

}
