package com.codewolf.commonserver.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @ClassName MybatisPlusConfig
 * @Description mybatisPlugin 配置类
 * @Author code wolf
 * @Date 2023/12/14 15:46
 * @Version 1.0
 */
@Configuration
@MapperScan({"com.codewolf.commonserver.mapper"})
@EnableTransactionManagement
public class MybatisConfig {
    /**
    * @MethodName mybatisPlusInterceptor
    * @Description   配置分页插件
     * @return: com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor
    * @Author code wolf
    * @Date 2023/12/14 15:49
    */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return mybatisPlusInterceptor;
    }
}
